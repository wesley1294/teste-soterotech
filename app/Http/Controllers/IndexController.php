<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IndexController extends Controller
{
    public function pessoa()
    {
          //clientes
          $pessoas = DB::table('pessoas')
          ->select('pessoas.*')->get();

          $pessoa1 = DB::table('pessoas')
          ->select('pessoas.*')->where('pessoas.id',1)->get();

          
          $pessoa2 = DB::table('pessoas')
          ->select('pessoas.*')->where('pessoas.id',2)->get();

          $pessoa3 = DB::table('pessoas')
          ->select('pessoas.*')->where('pessoas.id',3)->get();

          $pessoa4 = DB::table('pessoas')
          ->select('pessoas.*')->where('pessoas.id',4)->get();

          $pessoa5 = DB::table('pessoas')
          ->select('pessoas.*')->where('pessoas.id',5)->get();
          
        
             
          return view('index')
          ->with(compact('pessoas','pessoa1','pessoa2','pessoa3','pessoa4','pessoa5'));                      
    }

}
