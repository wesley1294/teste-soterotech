<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pessoa;
use Redirect;
use DB; 




class PessoaController extends Controller
{
    public function save(Request $req){

        if (DB::table('pessoas')->count('firstname') >= 5) {

        return Redirect::to("/")->withFail('Limite excedido!');
   
        }
        else{
            $pessoa = new Pessoa;
            $pessoa->firstname = $req->firstname;
            $pessoa->lastname = $req->lastname;
            $pessoa->participation = $req->participation;
            
          
            $pessoa->save();
            return Redirect::to("/")->withFail('Cadastrado com sucessso!');

        }
    }

}
