<!doctype html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SOTEROTECH</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <!-- Styles -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

        
    </head>
    @if(Session::has('fail'))
    <div class="alert alert-success">
       {{Session::get('fail')}}
    </div>
@endif

    <body>
    <div class="card text-white bg-primary col-md-12" >
  <div class="card-body">
  {!! Form:: open(['route'=>'cadastro.save', 'method' => 'POST', 'class'=>'form-inline']) !!}

<div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" required>
  </div>

  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" required>
  </div>

  <div class="form-group mx-sm-3 mb-2">
    <input type="number" class="form-control" id="Participation" name="participation" placeholder="Participation" required>
  </div>
  <button type="submit" class="btn btn-primary mb-2" style="border-color:white">Send</button>
  {!!Form:: close()!!} 

</div>
</div>

<div >
  <div class="card-body">
  <center> <b>DATA</b><br>
  Loren Ipsun
  </center>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
      <div class="card-body">
      <table class="table table-bordered  " style="margin-top:2%" >
  <thead>
  
    <tr>
    <th scope="row"></th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Participation</th>


    </tr>
   
  </thead>
  <tbody>
  @foreach($pessoas as $person)
    <tr>
      <td scope="row">{{ $person->id }}</td>
      <td>{{ $person->firstname }}</td>
      <td>{{ $person->lastname }}</td>
      <td>{{ $person->participation }} %</td>
    </tr>
    @endforeach
   
   
  </tbody>
</table>

     
    </div>
  </div>

  <div class="col-md-6">
    <div class="">
      <div class="card-body">
      <canvas id="pie-chart"   ></canvas>

      </div>
    </div>
  </div>
</div>



     
    </body>

    <script>
    
    new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: 
      @foreach($pessoa1 as $person1)
      ["{{ $person1->firstname }}"
      @foreach($pessoa2 as $person2)
      @foreach($pessoa3 as $person3)
      @foreach($pessoa4 as $person4)
      @foreach($pessoa5 as $person5)
      
      ,"{{ $person2->firstname }}"
      ,"{{ $person3->firstname }}"
      ,"{{ $person4->firstname }}"
      ,"{{ $person5->firstname }}"],
      
      
      datasets: [{
        label: "participation",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data: ["{{ $person1->participation }}","{{ $person2->participation }}","{{ $person3->participation }}","{{ $person4->participation }}","{{ $person5->participation }}"]
        @endforeach
        @endforeach
        @endforeach
        @endforeach
        @endforeach
        
      }]
    },

    
    options: {
      title: {
        display: true,
      }
    }
});
    </script>
</html>
